import 'package:flutter_mapbox_autocomplete/flutter_mapbox_autocomplete.dart'
    as autocomplete;

class MapBoxPlaceContext extends autocomplete.Context with MapBoxPlaceMixin {
  final int internalId;
  final String id;
  final String shortCode;
  final String wikiData;
  final String text;
  final String mapBoxPlaceId;

  MapBoxPlaceContext({
    this.internalId,
    this.id,
    this.shortCode,
    this.wikiData,
    this.text,
    this.mapBoxPlaceId,
  }) : super(
          id: id,
          text: text,
          shortCode: shortCode,
          wikidata: wikiData,
        );

  static MapBoxPlaceContext ofContext(autocomplete.Context context) {
    return MapBoxPlaceContext(
      id: context.id,
      shortCode: context.shortCode,
      wikiData: context.wikidata,
      text: context.text,
    );
  }

  static MapBoxPlaceContext fromJson(Map<String, dynamic> json) {
    var context = MapBoxPlaceContext(
      internalId: json['internalId'],
      id: json['id'],
      shortCode: json['shortCode'],
      wikiData: json['wikidata'],
      text: json['text'].toString(),
    );

    return context;
  }

  static MapBoxPlaceContext fromDBJson(Map<String, dynamic> json) {
    var context = MapBoxPlaceContext(
      internalId: json['internalId'],
      id: json['id'],
      mapBoxPlaceId: json['mapBoxPlaceId'],
      shortCode: json['short_code'],
      wikiData: json['wikidata'],
      text: json['text'].toString(),
    );

    return context;
  }

  Map<String, dynamic> toJsonDB(String mapBoxPlaceId) {
    return {
      "internalId": this.internalId,
      "id": this.id,
      "short_code": this.shortCode,
      "wikidata": this.wikidata,
      "text": this.text,
      "mapBoxPlaceId": mapBoxPlaceId,
    };
  }

  Map<String, dynamic> toJson() {
    return {
      // "internalId": this.internalId,
      "id": this.id,
      "shortCode": this.shortCode,
      "wikidata": this.wikidata,
      "text": this.text,
    };
  }
}

mixin MapBoxPlaceMixin on autocomplete.Context {
  int internalId;
  String mapBoxPlaceId;
}
