import 'dart:ui';

import 'package:flutter_mapbox_autocomplete/flutter_mapbox_autocomplete.dart';
import 'package:latlong/latlong.dart';

class Location {
  Location({
    this.id,
    this.position,
    this.color,
    this.me,
    this.address,
  });

  final int id;
  final LatLng position;
  final Color color;
  final bool me;
  final MapBoxPlace address;

  static latLngFromJson(Map<String, dynamic> jsonString) {
    if (jsonString == null) return;

    return LatLng(jsonString['_latitude'], jsonString['_longitude']);
  }

  getPlaceAddress() {
    if (address == null || address?.placeName == null) return null;

    final split = address.placeName.split(',');

    if (address.text != null) {
      var where = split.where((element) => element.contains(address.text));
      if (where != null && where.isNotEmpty) {
        return where.first;
      }
    }

    var indexOfAddress = split.length > 4 ? split.length - 4 : 0;

    if (split[indexOfAddress] != null &&
        split[indexOfAddress].contains(new RegExp('[0-9]'))) {
      return split[indexOfAddress];
    }

    return null;
  }

  getPostCode() {
    if ((address?.context ?? []).isEmpty) return null;

    var firstWhere = address.context.firstWhere(
      (element) => element.id.startsWith('postcode'),
      orElse: () => null,
    );
    return firstWhere?.text ?? null;
  }

  String getDistrict() {
    if ((address?.context ?? []).isEmpty) return null;

    var firstWhere = address.context.firstWhere(
      (element) => element.id.startsWith('locality'),
      orElse: () => null,
    );

    return firstWhere?.text ?? null;
  }

  String getCity() {
    if ((address?.context ?? []).isEmpty) return null;

    var firstWhere = address.context.firstWhere(
      (element) => element.id.startsWith('place'),
      orElse: () => null,
    );

    return firstWhere?.text ?? null;
  }

  String getRegion() {
    if ((address?.context ?? []).isEmpty) return null;

    var firstWhere = address.context.firstWhere(
      (element) => element.id.startsWith('region'),
      orElse: () => null,
    );

    return firstWhere?.text ?? null;
  }

  String getCountry() {
    if ((address?.context ?? []).isEmpty) return null;

    var firstWhere = address.context.firstWhere(
      (element) => element.id.startsWith('country'),
      orElse: () => null,
    );

    return firstWhere?.text ?? null;
  }

  String getCountryCode() {
    if ((address?.context ?? []).isEmpty || (address?.context?.length ?? 0) < 3)
      return null;

    var firstWhere = address.context.firstWhere(
      (element) => element.id.startsWith('country'),
      orElse: () => null,
    );

    return firstWhere?.shortCode ?? null;
  }

  List<String> getPlaceIdentifiers() {
    if (address?.context == null) {
      return null;
    }

    return address.context
        .map(
          (place) => getPlaceIdentifier(place),
        )
        .toList();
  }

  String getPlaceIdentifier(Context context) {
    if (context == null) {
      return null;
    }

    return context.id;
  }

  bool isValid({DataType dataType}) {
    if (address?.placeName != null &&
        position?.latitude != null &&
        position?.longitude != null) {
      if (dataType == null) {
        if (address.context == null) return false;
        if (address.context.isEmpty || address.context.length < 4) return false;
        var placeAddress = getPlaceAddress();
        var city = getCity();
        var country = getCountry();

        if (placeAddress == null || city == null || country == null)
          return false;
      }
    }

    return true;
  }
}

enum DataType {
  ADDRESS,
  ADDRESS_ENTITY,
  NUMBER,
  BIG_NUMBER,
  DATE,
  EMAIL,
  TELEPHONE,
  STRING,
  OPTIONS,
  PASSWORD,
  IMAGE,
  CHECKBOX,
}
