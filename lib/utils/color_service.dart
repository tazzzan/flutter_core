import 'package:flutter/material.dart';

class ColorService {
  static final ColorService _singleton = ColorService._internal();

  Color primary = primaryYellow;
  Color third = primaryGreenSilveredDark;

  static Color secondary = Color.fromRGBO(21, 32, 43, 1);

  static Color fontPrimary = Colors.white;
  static Color fontPrimaryUnfocused = Colors.white70;
  static Color fontSecondary = secondary;
  static Color fontSecondaryUnfocused = fontSecondary.withOpacity(0.4);

  static Color primaryYellow = Color.fromRGBO(226, 187, 34, 1);
  static Color primaryGrey = Color.fromRGBO(83, 84, 81, 1);
  static Color primaryOrange = Color.fromRGBO(176, 88, 58, 1);
  static Color primaryGreenSilvered = Color.fromRGBO(216, 220, 133, 1);
  static Color primaryGreenSilveredDark = Color.fromRGBO(174, 175, 133, 1);
  static Color primaryGreen = Color.fromRGBO(160, 218, 169, 1);
  static Color primaryGreenDark = Color.fromRGBO(0, 161, 112, 1);
  static Color primaryBlue = Color.fromRGBO(152, 180, 212, 1);
  static Color primaryBlueDark = Color.fromRGBO(18, 120, 185, 1);
  static Color primaryBlueLight = Color.fromRGBO(150, 223, 206, 1);

  static Color primaryRed = Color.fromRGBO(231, 74, 51, 1);
  static Color primaryRedDark = Color.fromRGBO(162, 36, 47, 1);

  static Color primaryPink = Color.fromRGBO(204, 58, 113, 1);
  static Color primaryPinkLight = Color.fromRGBO(237, 138, 129, 1);

  static Color primaryPurple =
      Color.fromRGBO(176, 159, 202, 1); // pantone - 15-3716

  static Color primaryInfo = Color.fromRGBO(0, 155, 153, 1);
  static Color primaryError = Color.fromRGBO(255, 89, 95, 1);

  factory ColorService() {
    return _singleton;
  }

  ColorService._internal();

  ColorService init() {
    return _singleton;
  }

  setPrimary(Color newPrimary) {
    this.primary = newPrimary;

    if (newPrimary == primaryYellow) {
      this.third = primaryGreenSilveredDark;
    }
    if (newPrimary == primaryGreen) {
      this.third = primaryGreenDark;
    }
    if (newPrimary == primaryRed) {
      this.third = primaryRedDark;
    }
    if (newPrimary == primaryRedDark) {
      this.third = primaryRed;
    }
    if (newPrimary == primaryPink) {
      this.third = primaryPinkLight;
    }
    if (newPrimary == primaryPurple) {
      this.third = Colors.white;
    }
    if (newPrimary == primaryBlue) {
      this.third = primaryBlueDark;
    }
  }
}
