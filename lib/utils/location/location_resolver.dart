import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';

class LocationResolver {
  static const int TIME_OUT_LENGTH_DEFAULT = 500;
  static const int TIME_OUT_LENGTH_POSITION = 1300;

  static Future<bool> isGranted({
    BuildContext context,
    bool enabledInDB,
  }) async {
    final locationServicesEnabled = await _locationServiceEnabled();

    if (locationServicesEnabled) {
      final LocationPermission permission =
          await Geolocator.checkPermission().catchError(
        (error) {
          print(error);
          _onError(
            context: context,
            message:
                'Error occurred when checking if geolocation is allowed. This app will use your registration address or the address of Hamburg to show the cars.',
          );
        },
      ).timeout(
        Duration(milliseconds: TIME_OUT_LENGTH_DEFAULT),
        onTimeout: () => _onError(
          context: context,
          message:
              'Could not proof if geolocation is allowed. This app will use your registration address or the address of Hamburg to show the cars.',
        ),
      );

      if (permission == null) {
        print('Could not proof if geolocation is allowed');
        return false;
      } else {
        return permission == LocationPermission.whileInUse ||
            permission == LocationPermission.always;
      }
    }

    return false;
  }

  static Future<bool> requestUserSelection({
    BuildContext context,
    openAlways = false,
  }) async {
    final locationServiceEnabled = await _locationServiceEnabled(
      context: context,
    );

    if (locationServiceEnabled) {
      final permission = await Geolocator.checkPermission();

      if (openAlways || permission == LocationPermission.deniedForever) {
        await requestAppSettings(context: context);
      } else {
        final currentPermission =
            await Geolocator.requestPermission().catchError(
          (error) {
            print(error);
            _onError(
              context: context,
              message:
                  'Error occurred when requesting manual selection of geolocation',
            );
          },
        );

        if (currentPermission == null) {
          print('Could not request manual selection of geolocation');
          return false;
        }

        return currentPermission == LocationPermission.always ||
            currentPermission == LocationPermission.whileInUse;
      }
    } else {
      await requestLocationSettings(context: context);
    }

    return false;
  }

  static Future<LatLng> getDevicePosition({BuildContext context}) async {
    final currentPosition = await Geolocator.getCurrentPosition(
      desiredAccuracy:
          Platform.isAndroid ? LocationAccuracy.low : LocationAccuracy.best,
    ).catchError((_) {
      print(_);
      _onError(
        context: context,
        message: 'Error occurred when getting geolocation from your phone',
      );
    }).timeout(
      Duration(milliseconds: TIME_OUT_LENGTH_POSITION),
      onTimeout: () async {
        final stopwatch = Stopwatch();
        stopwatch.start();

        final secondTry = await Geolocator.getCurrentPosition(
          desiredAccuracy:
              Platform.isAndroid ? LocationAccuracy.low : LocationAccuracy.best,
        ).catchError((_) {
          print(_);
          _onError(
            context: context,
            message: 'Error occurred when getting geolocation from your phone',
          );
        }).timeout(
          Duration(milliseconds: (TIME_OUT_LENGTH_POSITION * 2).toInt()),
          onTimeout: () async {
            stopwatch.stop();

            return _onError(
              context: context,
              message:
                  'Geolocation could not be obtained from your phone. Timeout: ' +
                      stopwatch.elapsedMilliseconds.toString() +
                      ' ms',
            );
          },
        );

        return secondTry ?? null;
      },
    );

    return currentPosition != null
        ? LatLng(
            currentPosition?.latitude,
            currentPosition?.longitude,
          )
        : null;
  }

  static requestAppSettings({BuildContext context}) async {
    await Geolocator.openAppSettings().catchError(
      (error) {
        print(error);
        _onError(
          context: context,
          message: 'Error occurred when opening settings of your phone',
        );
      },
    ).timeout(
      Duration(milliseconds: TIME_OUT_LENGTH_DEFAULT),
      onTimeout: () => _onError(
        context: context,
        message: 'The app could not open app settings',
      ),
    );
  }

  static requestLocationSettings({BuildContext context}) async {
    return await Geolocator.openLocationSettings().catchError(
      (error) {
        print(error);
        _onError(
          context: context,
          message:
              'Error occurred when opening location settings of your phone',
        );
      },
    ).timeout(
      Duration(milliseconds: TIME_OUT_LENGTH_DEFAULT),
      onTimeout: () => _onError(
        context: context,
        message: 'The app could not open location settings',
      ),
    );
  }

  static Future<bool> _locationServiceEnabled({BuildContext context}) async {
    final enabled = await Geolocator.isLocationServiceEnabled().catchError(
      (error) {
        print(error);
        _onError(
          context: context,
          message:
              'Error occurred when checking support of geolocation on your phone',
        );
      },
    ).timeout(
      Duration(milliseconds: TIME_OUT_LENGTH_DEFAULT),
      onTimeout: () => _onError(
        context: context,
        message: 'Your phone seems not to support geolocation',
      ),
    );

    if (enabled == null) {
      print('Location service is not enabled');
      return false;
    }

    return enabled;
  }

  static dynamic _onError({BuildContext context, String message}) {
    print('Error occurred: $message');

    ///

    return null;
  }
}
