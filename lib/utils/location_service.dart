import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter_core/utils/api/mapbox_api.dart';
import 'package:flutter_core/utils/location/location_resolver.dart';
import 'package:flutter_mapbox_autocomplete/flutter_mapbox_autocomplete.dart'
    as autocomplete;
import 'package:latlong/latlong.dart';

import 'model/location.dart';

class LocationService {
  static final LocationService _singleton = LocationService._internal();

  factory LocationService() {
    return _singleton;
  }

  LocationService._internal();

  LocationService init() {
    return _singleton;
  }

  static Future<List<Location>> getSuggestions(
    String query, {
    String type,
    int maxResults,
  }) async {
    final Completer<List<Location>> completer = new Completer();
    final String placeType = type != null ? 'types=$type' : '';
    final String languageCode = 'en';
    final String limit = 'limit=' +
        (maxResults != null
            ? maxResults.toString()
            : type != null
                ? '6'
                : '4');

    if (query == null || query.isEmpty) {
      completer.complete(null);
    } else {
      final String encodedQuery = Uri.encodeFull(query);
      final String apiKey = '';
      final String endPoint =
          '/geocoding/v5/mapbox.places/$encodedQuery.json?$placeType&access_token=$apiKey&cachebuster=1566806258853&autocomplete=true&language=$languageCode&$limit';

      MapBoxApi.server.get(
        endpoint: endPoint,
        onSuccess: (response) {
          autocomplete.Predections predections =
              autocomplete.Predections.fromRawJson(response);
          completer.complete(predections.features
              .map((feature) => Location(
                    position: LatLng(
                      feature.geometry.coordinates[1],
                      feature.geometry.coordinates.first,
                    ),
                    address: feature,
                  ))
              .toList());
        },
        onFailure: (error) => print(
          'Was not able to request suggestions at map_box. Error: $error ',
        ),
      );
    }

    return completer.future;
  }

  static Future<bool> isGranted({BuildContext context}) async {
    final bool granted = await LocationResolver.isGranted(
      context: context,
    );

    return granted ?? false;
  }

  static Future<bool> requestManualSelection({
    BuildContext context,
    alwaysOpenSettings = false,
  }) async {
    return await LocationResolver.requestUserSelection(
      context: context,
      openAlways: alwaysOpenSettings,
    );
  }

  static requestAppSettings({BuildContext context}) {
    LocationResolver.requestAppSettings(context: context);
  }

  static Future<Location> getCurrentAddress({BuildContext context}) {
    return LocationService.getCurrentPosition(
      context: context,
    ).then(
      (value) => LocationService.getAddress(value),
    );
  }

  static Future<LatLng> getCurrentPosition({BuildContext context}) async {
    LatLng currentPosition;

    final isGrantedGeolocation = await isGranted(context: context);
    if (isGrantedGeolocation) {
      currentPosition = await LocationResolver.getDevicePosition(
        context: context,
      );
    }

    return currentPosition ??
        LatLng(
          53.56,
          9.9,
        );
  }

  static Future<Location> getAddress(
    LatLng position, {
    BuildContext context,
  }) async {
    final Completer<Location> completer = new Completer();

    if (position == null) return null;

    if (position.longitude == null || position.latitude == null) {
      completer.complete(null);
      return completer.future;
    } else {
      final String longitude = Uri.encodeFull(position.longitude.toString());
      final String latitude = Uri.encodeFull(position.latitude.toString());
      final String placeType = 'types=address';
      final String languageCode = 'en';

      final String apiKey = '';

      final String endPoint =
          '/geocoding/v5/mapbox.places/$longitude,$latitude.json?$placeType&access_token=$apiKey&autocomplete=false&language=$languageCode';

      MapBoxApi.server.get(
          endpoint: endPoint,
          onSuccess: (response) {
            final List<autocomplete.MapBoxPlace> features =
                autocomplete.Predections.fromRawJson(
              response.body,
            ).features;

            final autocomplete.MapBoxPlace foundAddress =
                features != null && features.isNotEmpty ? features.first : null;

            if (foundAddress != null) {
              foundAddress.placeName.replaceAll(foundAddress.text + ', ', '');

              completer.complete(
                Location(
                  address: foundAddress,
                  position: position,
                ),
              );
            } else {
              !completer.isCompleted
                  ? completer.complete(position != null
                      ? Location(
                          address: null,
                          position: position,
                        )
                      : null)
                  : {};
            }
          },
          onFailure: (_) => completer.complete(
                position != null
                    ? Location(
                        address: null,
                        position: position,
                      )
                    : null,
              ));

      return completer.future;
    }
  }

  static double calcDistance(LatLng locationOne, LatLng locationTwo) {
    double lat1 = locationOne.latitude;
    double lon1 = locationOne.longitude;
    double lat2 = locationTwo.latitude;
    double lon2 = locationTwo.longitude;

    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  static LatLng calcCenter(List<LatLng> points) {
    double latitude = 0;
    double longitude = 0;
    int n = points.length;

    points.forEach((point) {
      latitude += point.latitude;
      longitude += point.longitude;
    });

    return new LatLng(latitude / n, longitude / n);
  }
}
