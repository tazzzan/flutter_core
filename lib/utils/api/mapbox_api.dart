import 'package:flutter_core/utils/api/api.dart';

class MapBoxApi {
  MapBoxApi._();

  static final Api server = Api.server(
    'api.mapbox.com',
    'https',
  );
}
