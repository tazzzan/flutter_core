import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Api {
  static const Map<String, String> HEADERS_APPLICATION_JSON = {
    'Content-Type': 'application/json'
  };

  Api._(this.domain, this.protocol, {this.port});

  static Api server(domain, protocol, {port}) {
    return Api._(domain, protocol, port: port);
  }

  final String domain;
  final String protocol;
  final int port;

  post({
    @required String endpoint,
    @required dynamic json,
    Function onSuccess,
    Function onFailure,
    bool extractFromJson = false,
    bool encodeJson = true,
    String extractFromJsonKey,
    Map<String, String> headers,
    int successResponseCode,
    int timeoutInSeconds,
  }) async {
    assert(json != null ? json is Map || json is Iterable : true);

    final Map<String, String> headersAdjusted = await _onInitHeaders(headers);

    final String url = '$protocol://$domain' +
        (port != null ? ':' + port.toString() : '') +
        endpoint;

    var stopwatch = Stopwatch();
    stopwatch.start();
    http
        .post(
          url,
          headers: headersAdjusted ?? HEADERS_APPLICATION_JSON,
          body: encodeJson
              ? utf8.encode(jsonEncode(
                  !extractFromJson ? json : json[extractFromJsonKey]))
              : !extractFromJson
                  ? json
                  : json[extractFromJsonKey],
        )
        .catchError((onError) => print(onError))
        .then(
      (response) {
        _onLogStopwatch(stopwatch, url);

        if (response.statusCode == (successResponseCode ?? 200)) {
          if (onSuccess != null) {
            onSuccess(utf8.decode(response.bodyBytes) ?? null);
          }
        } else {
          if (onFailure != null) {
            onFailure(response.statusCode);
          }
        }
      },
    ).timeout(
      Duration(seconds: timeoutInSeconds ?? 15),
      onTimeout: () {
        onFailure(408);
      },
    );
  }

  put({
    @required String endpoint,
    @required Map<String, dynamic> json,
    Function onSuccess,
    Function onFailure,
    Map<String, String> headers,
    int successResponseCode,
    int timeoutInSeconds,
  }) async {
    final Map<String, String> headersAdjusted = await _onInitHeaders(headers);

    final String url = '$protocol://$domain' +
        (port != null ? ':' + port.toString() : '') +
        endpoint;

    http
        .put(
      url,
      headers: headersAdjusted ?? HEADERS_APPLICATION_JSON,
      body: utf8.encode(jsonEncode(json)),
    )
        .then((response) {
      if (response.statusCode == (successResponseCode ?? 200)) {
        if (onSuccess != null) {
          onSuccess(utf8.decode(response.bodyBytes) ?? null);
        }
      } else {
        if (onFailure != null) {
          onFailure(response.statusCode);
        }
      }
    }).timeout(
      Duration(seconds: timeoutInSeconds ?? 30),
      onTimeout: () {
        onFailure(408);
      },
    );
  }

  get({
    @required String endpoint,
    Function onSuccess,
    Function onFailure,
    Map<String, String> headers,
    int timeoutInSeconds,
  }) async {
    final Map<String, String> headersAdjusted = await _onInitHeaders(headers);

    final String url = '$protocol://$domain' +
        (port != null ? ':' + port.toString() : '') +
        endpoint;

    var stopwatch = Stopwatch();
    stopwatch.start();
    http
        .get(
      url,
      headers: headersAdjusted ?? HEADERS_APPLICATION_JSON,
    )
        .then((response) {
      stopwatch.stop();
      print('REQUEST (GET) COMPLETE: ' +
          url +
          ' | ' +
          stopwatch.elapsedMilliseconds.toString() +
          'ms');

      if (response.statusCode == 200) {
        if (onSuccess != null) {
          onSuccess(utf8.decode(response.bodyBytes) ?? null);
        }
      } else {
        if (onFailure != null) {
          onFailure(response.statusCode);
        }
      }
    }).timeout(
      Duration(seconds: timeoutInSeconds ?? 15),
      onTimeout: () {
        onFailure(408);
      },
    );
  }

  delete({
    @required String endpoint,
    Function onSuccess,
    Function onFailure,
    int successResponseCode,
    Map<String, String> headers,
    int timeoutInSeconds,
  }) async {
    final Map<String, String> headersAdjusted = await _onInitHeaders(headers);

    final String url = '$protocol://$domain' +
        (port != null ? ':' + port.toString() : '') +
        endpoint;

    http
        .delete(
      url,
      headers: headersAdjusted ?? HEADERS_APPLICATION_JSON,
    )
        .then((response) {
      if (response.statusCode == (successResponseCode ?? 200)) {
        if (onSuccess != null) {
          onSuccess(response.body ?? null);
        }
      } else {
        if (onFailure != null) {
          onFailure(response.statusCode);
        }
      }
    }).timeout(
      Duration(seconds: timeoutInSeconds ?? 5),
      onTimeout: () {
        onFailure(408);
      },
    );
  }

  Future<Map<String, String>> _onInitHeaders(
      Map<String, String> headers) async {
    return headers ?? HEADERS_APPLICATION_JSON;
  }

  _onLogStopwatch(Stopwatch stopwatch, String url) {
    stopwatch.stop();
    print('REQUEST (POST) COMPLETE: ' +
        url +
        ' | ' +
        stopwatch.elapsedMilliseconds.toString() +
        'ms');
  }
}
