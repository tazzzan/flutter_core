import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core/utils/color_service.dart';

class WaitingSpinner extends StatelessWidget {
  const WaitingSpinner({
    Key key,
    this.color,
    this.isCentered = true,
  }) : super(key: key);

  final Color color;
  final bool isCentered;

  @override
  Widget build(BuildContext context) {
    final indicator = CircularProgressIndicator(
      valueColor: new AlwaysStoppedAnimation<Color>(
        color ?? ColorService.secondary,
      ),
    );

    return isCentered
        ? Center(
            child: indicator,
          )
        : indicator;
  }
}
