import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_core/utils/color_service.dart';
import 'package:flutter_core/widgets/background/background_gradient.dart';
import 'package:flutter_core/widgets/waiting/waiting_spinner.dart';

class WaitingView extends StatelessWidget {
  const WaitingView({
    Key key,
    this.noBackground = false,
    this.isCentered = true,
    this.popAllowed = true,
    this.title,
    this.colors,
    this.image,
  }) : super(key: key);

  final List<Color> colors;
  final String title;
  final Uint8List image;
  final bool noBackground;
  final bool isCentered;
  final bool popAllowed;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final container = Container(
      child: Stack(
        children: [
          ...!noBackground && colors != null
              ? [
                  BackgroundGradient(
                    colors: colors,
                  )
                ]
              : [],
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(top: size.height * 0.19),
              child: Text(
                title ?? '',
                style: TextStyle(
                  color: ColorService.fontPrimary,
                  fontSize: 20,
                  letterSpacing: 1.2,
                ),
              ),
            ),
          ),
          image != null
              ? Positioned(
                  top: size.height * 0.24,
                  left: size.width * 0.125,
                  child: Container(
                    width: size.width * 0.75,
                    height: size.width * 0.75 * 0.49,
                    decoration: ShapeDecoration(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(35.0),
                        ),
                        side: BorderSide(
                          color: Colors.white24,
                          width: 0.5,
                        ),
                      ),
                      image: new DecorationImage(
                        fit: BoxFit.cover,
                        image: MemoryImage(
                          image,
                        ),
                      ),
                    ),
                  ),
                )
              : Container(),
          WaitingSpinner(),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.only(top: size.height * 0.25),
              child: FlatButton(
                color: ColorService.secondary,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(35.0),
                  ),
                ),
                textColor: ColorService.fontPrimary,
                child: Text('Cancel'),
                onPressed: () => popAllowed ? Navigator.pop(context) : {},
              ),
            ),
          ),
        ],
      ),
    );

    return isCentered ? Center(child: container) : container;
  }
}
