import 'package:flutter/material.dart';
import 'package:flutter_core/utils/color_service.dart';
import 'package:flutter_core/widgets/buttons/circle_button_raised.dart';

class CircleButtonRaisedStated extends StatefulWidget {
  const CircleButtonRaisedStated({
    Key key,
    @required this.child,
    @required this.color,
    this.onTap,
    this.centeredLeft = false,
    this.anySelected = false,
    this.disabledColor,
    this.onLongPress,
    this.onUnselect,
    this.size,
  }) : super(key: key);

  final bool centeredLeft;
  final Function onUnselect;
  final Function onTap;
  final Function onLongPress;
  final bool anySelected;
  final Color color;
  final Color disabledColor;
  final Widget child;
  final double size;

  @override
  CircleButtonRaisedStatedState createState() =>
      CircleButtonRaisedStatedState();
}

class CircleButtonRaisedStatedState extends State<CircleButtonRaisedStated> {
  double size;
  Widget child;
  Function onTap;

  @override
  Widget build(BuildContext context) {
    size = widget.size ?? 25;

    if (child == null) {
      child = widget.child;
    }

    if (onTap == null) {
      onTap = widget.onTap;
    }

    return CircleButtonRaised(
      child: size != null && size != 0 ? child : Container(),
      size: size,
      color: widget.color,
      disabledColor:
          widget.disabledColor ?? ColorService.secondary.withOpacity(0.55),
      onSelect: _onSelect(widget.anySelected),
    );
  }

  Offset getCurrentOffset(BuildContext context) {
    RenderBox renderBox = context.findRenderObject();

    return renderBox.globalToLocal(Offset.zero);
  }

  setChild(Widget newChild) {
    setState(() {
      child = newChild;
    });
  }

  setOnTap(Function newOnTap) {
    setState(() {
      onTap = newOnTap;
    });
  }

  Function _onSelect(bool selected) {
    return widget.onUnselect != null
        ? widget.onUnselect
        : selected && onTap != null
            ? onTap
            : null;
  }
}
