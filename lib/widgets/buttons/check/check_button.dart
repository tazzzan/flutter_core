import 'package:flutter/cupertino.dart';
import 'package:flutter_core/utils/color_service.dart';
import 'package:flutter_core/widgets/buttons/circle_button_raised_stated.dart';
import 'package:flutter_core/widgets/circle/circle.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class CheckButton extends StatefulWidget {
  const CheckButton({
    Key key,
    @required this.size,
    this.isIconCheckMark = true,
    this.isChecked = false,
    this.onSelect,
  }) : super(key: key);

  final double size;
  final bool isIconCheckMark;
  final bool isChecked;
  final Function onSelect;

  @override
  CheckButtonState createState() => CheckButtonState();
}

class CheckButtonState extends State<CheckButton> {
  final GlobalKey<CircleButtonRaisedStatedState> keyButton = GlobalKey();

  var checked;
  var checkAllowed = true;

  @override
  Widget build(BuildContext context) {
    if (checked == null) {
      checked = widget.isChecked;
    }

    return CircleButtonRaisedStated(
      key: keyButton,
      size: widget.size,
      color: ColorService.fontSecondary,
      disabledColor: ColorService.fontSecondary,
      onTap: () => checkAllowed
          ? !checked
              ? onCheck()
              : onUnCheck()
          : {},
      child: checked
          ? SizedBox(
              height: widget.size * 0.75,
              width: widget.size * 0.75,
              child: widget.isIconCheckMark
                  ? Icon(
                      MdiIcons.check,
                      size: widget.size * 0.6,
                      color: ColorService.primaryGreenDark,
                    )
                  : Circle(),
            )
          : Container(),
    );
  }

  onCheck() {
    checked = true;

    keyButton.currentState != null
        ? keyButton.currentState.setChild(
            SizedBox(
              height: widget.size * 0.75,
              width: widget.size * 0.75,
              child: widget.isIconCheckMark
                  ? Icon(
                      MdiIcons.check,
                      size: widget.size * 0.8,
                      color: ColorService.primaryGreenDark,
                    )
                  : Circle(),
            ),
          )
        : {};

    widget.onSelect != null ? widget.onSelect(true) : {};
  }

  onUnCheck() {
    checked = false;

    keyButton.currentState != null
        ? keyButton.currentState.setChild(Container())
        : {};

    widget.onSelect != null ? widget.onSelect(false) : {};
  }

  onSetCheckAllowed(bool allowed) {
    setState(() {
      checkAllowed = allowed;
    });
  }
}
