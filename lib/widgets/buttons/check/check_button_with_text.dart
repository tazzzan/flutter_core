import 'package:flutter/cupertino.dart';
import 'package:flutter_core/utils/color_service.dart';

import 'check_button.dart';

class CheckButtonWithText extends StatefulWidget {
  const CheckButtonWithText({
    Key key,
    @required this.size,
    @required this.text,
    this.isIconCheckMark = true,
    this.isChecked = false,
    this.onSelect,
  }) : super(key: key);

  final double size;
  final String text;
  final bool isIconCheckMark;
  final bool isChecked;
  final Function onSelect;

  @override
  CheckButtonWithTextState createState() => CheckButtonWithTextState();
}

class CheckButtonWithTextState extends State<CheckButtonWithText> {
  final GlobalKey<CheckButtonState> keyCheckButton = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: CheckButton(
            key: keyCheckButton,
            isChecked: widget.isChecked,
            isIconCheckMark: widget.isIconCheckMark,
            size: widget.size,
            onSelect: widget.onSelect,
          ),
        ),
        Text(
          widget.text ?? '',
          style: TextStyle(
            fontSize: 12,
            color: ColorService.fontSecondary,
          ),
        )
      ],
    );
  }

  onSetCheckAllowed(bool allowed) {
    keyCheckButton.currentState != null
        ? keyCheckButton.currentState.onSetCheckAllowed(allowed)
        : {};
  }

  onSetChecked(bool checked) {
    keyCheckButton.currentState != null
        ? checked
            ? keyCheckButton.currentState.onCheck()
            : keyCheckButton.currentState.onUnCheck()
        : {};
  }

  bool isChecked() {
    return keyCheckButton.currentState != null
        ? keyCheckButton.currentState.checked
        : false;
  }
}
