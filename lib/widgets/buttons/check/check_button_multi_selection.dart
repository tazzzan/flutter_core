import 'package:flutter/cupertino.dart';
import 'package:flutter_core/widgets/buttons/check/check_button_with_text.dart';

class CheckButtonMultiSelection extends StatefulWidget {
  const CheckButtonMultiSelection({
    Key key,
    @required this.optionHeight,
    @required this.options,
    @required this.onSelect,
    this.selectedOption,
  }) : super(key: key);

  final double optionHeight;
  final List<String> options;
  final Function onSelect;
  final String selectedOption;

  CheckButtonMultiSelectionState createState() =>
      CheckButtonMultiSelectionState();
}

class CheckButtonMultiSelectionState extends State<CheckButtonMultiSelection> {
  List<GlobalKey<CheckButtonWithTextState>> keyCheckButtons = [];

  String selectedOption;

  @override
  void initState() {
    widget.options.forEach((element) {
      keyCheckButtons.add(GlobalKey());
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (selectedOption == null) {
      selectedOption = widget.selectedOption;
    }

    return Column(
        children: widget.options.map(
      (option) {
        final indexOfOption = widget.options.indexOf(option);

        return Padding(
          padding: EdgeInsets.symmetric(
            vertical: widget.optionHeight * 0.2,
          ),
          child: CheckButtonWithText(
            key: keyCheckButtons[indexOfOption],
            text: option,
            isChecked: option == selectedOption,
            size: widget.optionHeight,
            isIconCheckMark: true,
            onSelect: (checked) => onCheck(checked, option),
          ),
        );
      },
    ).toList());
  }

  void onCheck(bool checked, String option) {
    checked
        ? selectedOption = option
        : option == selectedOption
            ? selectedOption = null
            : {};

    widget.onSelect != null ? widget.onSelect(selectedOption) : {};

    checked
        ? keyCheckButtons
            .where((element) => element.currentState != null)
            .where((element) => element.currentState.widget.text != option)
            .forEach((element) => element.currentState.onSetChecked(false))
        : {};
  }
}
