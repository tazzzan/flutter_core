import 'package:flutter/material.dart';
import 'package:flutter_core/utils/color_service.dart';

class CircleButtonRaised extends StatelessWidget {
  const CircleButtonRaised({
    @required this.child,
    @required this.color,
    @required this.size,
    this.anySelected = false,
    this.onUnselect,
    this.disabledColor,
    this.onSelect,
  });

  final double size;
  final Function onSelect;
  final Function onUnselect;
  final bool anySelected;
  final Color color;
  final Color disabledColor;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: size,
      height: size,
      child: GestureDetector(
        onTap: _onSelect(!anySelected),
        child: RaisedButton(
          padding: EdgeInsets.all(0),
          disabledColor:
              disabledColor ?? ColorService.secondary.withOpacity(0.55),
          color: color,
          shape: CircleBorder(),
          onPressed: _onSelect(anySelected),
          child: size != 0 ? child : Container(),
        ),
      ),
    );
  }

  Offset getCurrentOffset(BuildContext context) {
    RenderBox renderBox = context.findRenderObject();

    return renderBox.globalToLocal(Offset.zero);
  }

  Function _onSelect(bool selected) {
    return onUnselect != null
        ? onUnselect
        : selected && onSelect != null
            ? onSelect
            : {};
  }
}
