import 'package:flutter/cupertino.dart';
import 'package:flutter_core/utils/color_service.dart';

class Circle extends StatelessWidget {
  const Circle({
    Key key,
    this.color,
    this.child,
    this.size,
  }) : super(key: key);

  final Color color;
  final Widget child;
  final double size;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      decoration: ShapeDecoration(
        color: color ?? ColorService.primaryGreenDark,
        shape: CircleBorder(),
      ),
      child: child ?? Container(),
    );
  }
}
