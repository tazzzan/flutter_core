import 'package:flutter/material.dart';
import 'package:flutter_core/widgets/blur/blur_transition.dart';

class BlurOverlay extends StatefulWidget {
  BlurOverlay({
    Key key,
    this.child,
  }) : super(key: key);

  final Widget child;

  @override
  BlurOverlayState createState() => BlurOverlayState();
}

class BlurOverlayState extends State<BlurOverlay>
    with SingleTickerProviderStateMixin {
  static const int DURATION_ANIMATION = 300;
  static const double ANIMATION_START = 0.0;
  static const double ANIMATION_END = 10.0;

  Animation blurAnimation;
  AnimationController blurAnimationController;

  @override
  void initState() {
    blurAnimationController = new AnimationController(
        vsync: this,
        duration: Duration(
          milliseconds: DURATION_ANIMATION,
        ));

    blurAnimation = CurvedAnimation(
      curve: Curves.linear,
      parent: blurAnimationController,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlurTransition(
      animation: new Tween<double>(
        begin: ANIMATION_START,
        end: ANIMATION_END,
      ).animate(blurAnimation),
      child: widget.child,
    );
  }
}
