import 'package:flutter/material.dart';
import 'package:flutter_core/widgets/blur/blur_transition.dart';

class BlurNavOverlay extends PopupRoute<Null> {
  BlurNavOverlay({
    this.child,
  });

  static const int DURATION_ANIMATION = 300;
  static const double ANIMATION_START = 0.0;
  static const double ANIMATION_END = 10.0;

  final Widget child;

  @override
  Color get barrierColor => null;

  @override
  bool get barrierDismissible => true;

  @override
  String get barrierLabel => "Close";

  @override
  Duration get transitionDuration {
    return const Duration(milliseconds: DURATION_ANIMATION);
  }

  @override
  Widget buildTransitions(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) =>
      new BlurTransition(
        animation: new Tween<double>(
          begin: ANIMATION_START,
          end: ANIMATION_END,
        ).animate(animation),
        child: Scaffold(
          body: child,
          backgroundColor: Colors.transparent,
        ),
      );

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) =>
      child;
}
