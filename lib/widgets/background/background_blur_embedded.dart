import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_core/utils/color_service.dart';

class BackgroundBlurEmbedded extends StatelessWidget {
  BackgroundBlurEmbedded({
    Key key,
    @required this.child,
    @required this.size,
    this.color,
    this.margin,
  }) : super(key: key);

  final Widget child;
  final Size size;
  final Color color;
  final EdgeInsets margin;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size.height,
      width: size.width,
      margin: margin ?? EdgeInsets.zero,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(25)),
        color: color ?? ColorService.primaryGrey,
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(25)),
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 11,
            sigmaY: 11,
          ),
          child: child,
        ),
      ),
    );
  }
}
