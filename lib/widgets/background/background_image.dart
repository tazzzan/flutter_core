import 'package:flutter/material.dart';

class BackgroundImage extends StatelessWidget {
  BackgroundImage({
    @required this.image,
    this.opacity = 1.0,
    this.crop = false,
  });

  final double opacity;
  final bool crop;
  final AssetImage image;

  @override
  Widget build(BuildContext context) {
    final borderRadius = BorderRadius.all(
      Radius.circular(35.0),
    );

    return Opacity(
      child: Container(
        decoration: BoxDecoration(
          image: image != null
              ? new DecorationImage(
                  fit: BoxFit.cover,
                  image: image,
                )
              : null,
          borderRadius: borderRadius,
        ),
      ),
      opacity: opacity ?? 0.6,
    );
  }
}
