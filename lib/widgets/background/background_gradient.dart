import 'package:flutter/material.dart';
import 'package:flutter_core/utils/color_service.dart';

class BackgroundGradient extends StatelessWidget {
  BackgroundGradient({
    @required this.colors,
    this.child,
    this.background = false,
    this.offset,
    this.borderRadius,
  }) : assert(colors.length == 3);

  final List<Color> colors;
  final Widget child;
  final bool background;
  final double offset;
  final BorderRadius borderRadius;

  @override
  Widget build(BuildContext context) {
    var border = borderRadius != null
        ? borderRadius
        : BorderRadius.all(
            Radius.circular(0),
          );

    final Widget gradient = Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment(Alignment.topCenter.x - 1.5, Alignment.topCenter.y),
          colors: colors,
          stops: [
            0.1,
            0.675,
            1,
          ],
        ),
        borderRadius: border,
      ),
      child: child ?? Container(),
    );

    if (!background) {
      return gradient;
    }

    border = BorderRadius.only(
      bottomLeft: Radius.circular(35.0),
      bottomRight: Radius.circular(35.0),
    );

    return ClipRect(
      child: Stack(
        children: [
          background
              ? Container(
                  color: ColorService().primary,
                )
              : Container(),
          gradient,
        ],
      ),
    );
  }
}
